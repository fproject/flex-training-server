<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Flex Training',

    // preloading 'log' component
    'preload' => array(
        'log',
        'zend'),

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.components.services.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
    ),

    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '12345',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),

        'rights'=>array(
            'superuserName'=>'RBACAdmin', // Name of the role with super user privileges.
            'authenticatedName'=>'Authenticated', // Name of the authenticated user role.
            'userIdColumn'=>'id', // Name of the user id column in the database.
            'userNameColumn'=>'userName', // Name of the user name column in the database.
            'enableBizRule'=>true, // Whether to enable authorization item business rules.
            'enableBizRuleData'=>false, // Whether to enable data for business rules.
            'displayDescription'=>true, // Whether to use item description instead of name.
            'flashSuccessKey'=>'RightsSuccess', // Key to use for setting success flash messages.
            'flashErrorKey'=>'RightsError', // Key to use for setting error flash messages.
            'install'=>true, // Whether to allow installing/re-installing Rights module.
            'baseUrl'=>'/rights', // Base URL for Rights. Change if module is nested.
            'layout'=>'rights.views.layouts.main', // Layout to use for displaying Rights.
            'appLayout'=>'application.views.layouts.main', // Application layout.
            'cssFile'=>'rights.css', // Style sheet file to use for Rights.
            'debug'=>false, // Whether to enable debug mode.
        ),

        'amfGateway' => array(
            'servicesDirAlias' => 'application.services.amf',
            'productionMode' => false,
            'amfDiscoveryEnabled' => true,
            'allowAnonymousAccess' => true,
        ),

        // The simple Workflow source module
        'swSource'=> array(
            'class'=>'application.modules.simpleWorkflow.SWPhpWorkflowSource',
            'workflowPath'=>'application.modules.simpleWorkflow.workflows',
        ),
    ),

    // application components
    'components' => array(
        'user' => array(
            'class'=>'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,

            //The number of seconds in which the user token will expire. Default is one day.
            'tokenExpire' => 86400,
        ),

        // uncomment the following to enable URLs in path-format
        'urlManager'=>array(
            'showScriptName'=>false,
            /*'urlFormat'=>'path',
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),*/
        ),

        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=flextraining',//'mysql:host=localhost;dbname=flextraining',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            //'enableParamLogging'=>true,
        ),

        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),

        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, trace',//'levels'=>'trace, info, error, warning, application',
                    //'showInFireBug'=>true, //firebug only - turn off otherwise
                    'maxFileSize' => 1024,//maximum log file size in kilo-bytes (KB).
                ),

                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),

        'cache'=>array(
            'class'=>'system.caching.CDbCache',
        ),

        'authManager' => array(
            'class' => 'RDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'f_auth_item',
            'itemChildTable' => 'f_auth_item_child',
            'assignmentTable' => 'f_auth_assignment',
            'rightsTable'=>'f_rights',
        ),

        //UserManager component
        'userManager' => array(
            'class'=>'UserManager',

            # send activation email
            'sendActivationMail' => true,

            # allow access for non-activated users
            'allowInactivatedLogin' => false,

            # activate user on registration (only sendActivationMail = false)
            'activateAfterRegister' => false,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        //DateTime format use for DB
        'dbDateTimeFormat'=>'Y-m-d H:i:s',

        //The method and password for data encryption
        'encryptionMethod' => 'aes256',
        'encryptionPassword' => 'test_password_1234',

        // this URL is used to redirect after user logged in successfully
        'loginSuccessUrl' => '?r=user',

        // this is used in contact page
        'adminEmail' => 'training@f-project.net',

        //these two params are used in FlexWidget to display Apache Flex application
        'flexAppBasePath' => '/flexapps/flextraining',
        'flexRSLBasePath' => '/flexapps/rsls',
        'flexModuleBasePath' => '/flexapps/flextraining/modules',
        //The path to static resource folder
        'flexResourceBaseUrl' => '/flexapps/flextraining/resources',

        //The folder to save attached files using AttachmentService
        'attachmentBaseUrl' => '/fattachments',
    ),

    //Tạm thời hard code timezone. Giá trị này cần được set sau khi user login vào hệ thống, dựa theo
    //setting của từng user khác nhau sẽ có timezone khác nhau
    'timeZone' => 'Asia/Ho_Chi_Minh',
    'language'=>'vi_vn'
);