<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

Yii::import('application.services.amf.*');
Yii::import('application.services.vo.*');
/**
 * This service is exposed to AMF clients to handle AppContext information
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
class EmployeeService
{

    /**
     * Get an employee by ID.
     * @param string $id
     * @return FEmployee
     */
    public function getEmployee($id)
	{
        return FEmployee::staticInstance()->findByPk($id);
	}

    /**
     * Get all employees.
     * @return FEmployee[]
     */
    public function getAllEmployees()
    {
        return FEmployee::staticInstance()->findAll();
	}

    /**
     * Save an employee
     * @param FEmployee $employee the employee to save
     */
    public function saveEmployee($employee)
    {
        $employee->save();
    }
}