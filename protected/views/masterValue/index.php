<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/* @var $this MasterValueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Values',
);

$this->menu=array(
	array('label'=>'Create MasterValue', 'url'=>array('create')),
	array('label'=>'Manage MasterValue', 'url'=>array('admin')),
);
?>

<h1>Master Values</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
