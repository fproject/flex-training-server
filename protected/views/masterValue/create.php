<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/* @var $this MasterValueController */
/* @var $model MasterValue */

$this->breadcrumbs=array(
	'Master Values'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MasterValue', 'url'=>array('index')),
	array('label'=>'Manage MasterValue', 'url'=>array('admin')),
);
?>

<h1>Create MasterValue</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>