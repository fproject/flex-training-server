<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/* @var $this MasterValueController */
/* @var $model MasterValue */

$this->breadcrumbs=array(
	'Master Values'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List MasterValue', 'url'=>array('index')),
	array('label'=>'Create MasterValue', 'url'=>array('create')),
	array('label'=>'Update MasterValue', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MasterValue', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MasterValue', 'url'=>array('admin')),
);
?>

<h1>View MasterValue #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'typeId',
		'languageId',
		'name',
		'description',
		'createTime',
		'createUserId',
		'updateTime',
		'updateUserId',
	),
)); ?>
