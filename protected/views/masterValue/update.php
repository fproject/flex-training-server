<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/* @var $this MasterValueController */
/* @var $model MasterValue */

$this->breadcrumbs=array(
	'Master Values'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MasterValue', 'url'=>array('index')),
	array('label'=>'Create MasterValue', 'url'=>array('create')),
	array('label'=>'View MasterValue', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MasterValue', 'url'=>array('admin')),
);
?>

<h1>Update MasterValue <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>