<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/* @var $this MainAppController */
/* @var $projectId integer */
/* @var $loginUserId integer */
/* @var $userToken string */

$this->pageTitle='ProjectKit';

Yii::app()->clientScript->registerScript('mainApp.open',"
$('#page').css('width','100%').css('height','100%').css('margin-top','0px').css('margin-bottom','0px').css('margin-left','0px').css('margin-right','0px').css('border','0px');
$('#content').css('width','100%').css('height','100%').css('top','0px').css('bottom','0px').css('left','0px').css('right','0px').css('padding-top','0px').css('padding-right','0px').css('padding-left','0px').css('padding-bottom','0px');
$('#header').remove();
$('#mainmenu').remove();
$('#footer').remove();
");

/** @var string $baseUrl */
$baseUrl = Yii::app()->baseUrl;
$this->widget('CFlexWidget', array(
        'name'=>'ProjectKit',
        'baseUrl'=>$baseUrl.Yii::app()->params['flexAppBasePath'],
        'rslBaseUrl'=>$baseUrl.Yii::app()->params['flexRSLBasePath'],
        'moduleBaseUrl'=>$baseUrl.Yii::app()->params['flexModuleBasePath'],
        'width'=>'100%',
        'height'=>'100%',
        'align'=>'left',
        'enableHistory'=>'false',
        'allowFullScreen'=>'true',
        'allowFullScreenInteractive'=>'true',
        'flashVars'=>array(
            'projectId'=>$projectId,
            'loginUserId'=>$loginUserId,
            'userToken'=>str_replace('"', '\\"', $userToken),
        ),
    )
);