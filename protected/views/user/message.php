<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/* @var $this UserController */

$this->pageTitle=Yii::app()->name . ' - '."Login";
?>

<h1><?php echo $title; ?></h1>

<div class="form">
<?php echo $content; ?>

</div><!-- yiiForm -->