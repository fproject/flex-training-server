<?php 
////////////////////////////////////////////////////////////////////////////////////////
// This simpleWorkflow definition file was generated automatically
// from a yEd Graph Editor file (.graphml).
//
// Workflow Name : swIssue
// Created       : 27/08/2014 17:47

return array(
	'initial' => Issue::STATUS_DRAFT,
	'node'    => array(
		array(
			'id' => Issue::STATUS_DRAFT,
			'transition' => array(
				Issue::STATUS_OPEN,
				Issue::STATUS_DELETED,
			),
			'metadata' => array(
				'background-color' => '#FFFFFF',
				'color' => '#000000',
			),
		),
		array(
			'id' => Issue::STATUS_OPEN,
			'transition' => array(
				Issue::STATUS_IN_PROGRESS,
				Issue::STATUS_CANCELLED,
			),
			'metadata' => array(
				'background-color' => '#FF6600',
				'color' => '#000000',
			),
		),
		array(
			'id' => Issue::STATUS_IN_PROGRESS,
			'transition' => array(
				Issue::STATUS_RESOLVED,
				Issue::STATUS_CANCELLED,
			),
			'metadata' => array(
				'background-color' => '#FF99CC',
				'color' => '#000000',
			),
		),
		array(
			'id' => Issue::STATUS_RESOLVED,
			'constraint' => '!$this->isEscalating()',
			'transition' => array(
				Issue::STATUS_CLOSED => 'self::sendMail()',
				Issue::STATUS_CANCELLED,
				Issue::STATUS_IN_PROGRESS,
			),
			'metadata' => array(
				'background-color' => '#99CC00',
				'color' => '#000000',
			),
		),
		array(
			'id' => Issue::STATUS_CANCELLED,
			'transition' => array(
				Issue::STATUS_OPEN,
			),
			'metadata' => array(
				'background-color' => '#FFCC00',
				'color' => '#000000',
			),
		),
		array(
			'id' => Issue::STATUS_DELETED,
			'metadata' => array(
				'background-color' => '#000000',
				'color' => '#FFFFFF',
			),
		),
		array(
			'id' => Issue::STATUS_CLOSED,
			'metadata' => array(
				'background-color' => '#00FF00',
				'color' => '#000000',
			),
		),
	)
);
