<?php
/**
 * Exception thrown by the simpleWorkflow behavior
 */
class SWException extends CException {
	const SW_ERR_ATTR_NOT_FOUND=1;
	const SW_ERR_REENTRANCE=2;
	const SW_ERR_WRONG_TYPE=3;
	const SW_ERR_IN_WORKFLOW=4;
	const SW_ERR_CREATE_FAILS=5;
	const SW_ERR_WRONG_STATUS=6;
	const SW_ERR_WORKFLOW_NOT_FOUND=7;
	const SW_ERR_WORKFLOW_ID=8;
	const SW_ERR_NODE_NOT_FOUND=9;
	const SW_ERR_STATUS_UNREACHABLE=10;
	
	const SW_ERR_CREATE_NODE=11;
	const SW_ERR_MISSING_NODE_ID=12;
}
?>