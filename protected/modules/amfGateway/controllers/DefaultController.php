<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
class DefaultController extends AmfController
{
	public function actionIndex() {
		$this->productionMode = Yii::app()->getModule('amfGateway')->productionMode;
        $this->amfDiscoveryEnabled = Yii::app()->getModule('amfGateway')->amfDiscoveryEnabled;
        $this->allowAnonymousAccess = Yii::app()->getModule('amfGateway')->allowAnonymousAccess;
		$servicesFolder = Yii::getPathOfAlias(
			Yii::app()->getModule('amfGateway')->servicesDirAlias);
		$this->handle($servicesFolder);
		return $servicesFolder;
	}	
}