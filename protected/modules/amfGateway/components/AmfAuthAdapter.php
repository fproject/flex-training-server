<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 *
 * The AmfAuth class is used for AMF Service authentication.
 *
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
class AmfAuthAdapter extends Zend_Amf_Auth_Abstract{

    /**
     * Performs an authentication attempt
     *
     * @throws Zend_Auth_Adapter_Exception If authentication cannot be performed
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
        $identity = new AmfUserIdentity($this->_username, $this->_password);

        if(ctype_digit(strval($this->_username)))
        {
            $identity->token = $this->_password;
        }

        if($identity->authenticate())
        {
            $identity->role = AmfAcl::DEFAULT_LOGIN_ROLE;
            $code = Zend_Auth_Result::SUCCESS;
        }
        else
        {
            switch($identity->errorCode)
            {
                case AmfUserIdentity::ERROR_INVALID_TOKEN:
                case AmfUserIdentity::ERROR_PASSWORD_INVALID:
                    $code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
                    break;
                case AmfUserIdentity::ERROR_USERNAME_INVALID:
                    $code = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
                    break;
                default:
                    $code = Zend_Auth_Result::FAILURE;
            }
        }
        return new Zend_Auth_Result($code, $identity);
    }

    /**
     * The Acl used to control AMF authorization
     * @return AmfAcl
     */
    public function getAcl()
    {
        return AmfAcl::staticInstance();
    }
}