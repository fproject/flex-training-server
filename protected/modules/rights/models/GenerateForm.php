<?php
/**
* Generation form class file.
*
* @author Christoffer Niska <cniska@live.com>
* @copyright Copyright &copy; 2010 Christoffer Niska
* @since 0.9.8
*/
class GenerateForm extends CFormModel
{
    /** @var array $items */
	public $items;
    /** @var array $parentControllers */
    public $parentControllers;//20130607 Added : ProjectKit.net customization

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('items', 'safe'),
		);
	}
}
