<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 * User Account Activation Action
 * @property UserController $controller
 */
class Activation extends CAction
{
    public function run()
    {
        /** @var UserController $controller */
        $controller = $this->getController();

        $email = $_GET['email'];
        $activateKey = $_GET['activateKey'];

        if (!is_null($email) && !is_null($activateKey))
        {
            /** @var User $find */
            $find = User::model()->unsafe()->findByAttributes(array('email'=>$email));

            if ($find->status == User::STATUS_ACTIVATED)
            {
                $controller->render('/user/message'
                    ,array('title'=>"User activation",
                        'content'=>"You account is already activated. Click ".CHtml::link("here",["site/login"]).' to login.'));
            }
            elseif (UserManager::validateActivationKey($activateKey,$find->activateKey))
            {
                //$find->activateKey = microtime();
                $find->status = User::STATUS_ACTIVATED;
                if($find->update(['status']))
                    $controller->render('/user/message',
                        array('title'=>"User activation",
                            'content'=>"You account is now activated. Click ".CHtml::link("here",["site/login"]).' to login.'));
                else
                    $controller->render('/user/message',
                        array('title'=>"User activation",
                            'content'=>"An error occurred while activating your account. Please try again later."));
            }
            else
            {
                $controller->render('/user/message',
                    array('title'=>"User activation",
                        'content'=>"Incorrect activation URL."));
            }
        }
        else
        {
            $controller->render('/user/message',
                array('title'=>"User activation",
                    'content'=>"Incorrect activation URL."));
        }
    }
} 