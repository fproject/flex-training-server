<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 * User Account Recovery Action
 *
 * @property UserController $controller
 *
 */
class ChangePassword extends CAction
{
    public function run()
    {
        /** @var UserController $controller */
        $controller = $this->getController();

        if (Yii::app()->user->id)
        {
            $form = new UserChangePasswordForm();
            if(isset($_POST['UserChangePasswordForm']))
            {
                $form->attributes=$_POST['UserChangePasswordForm'];
                if($form->validate())
                {
                    /** @var User $model */
                    $model = User::model()->unsafe()->findbyPk(Yii::app()->user->id);
                    if($model->validatePassword($form->oldPassword))
                    {
                        $model->password = $form->password;
                        $model->activateKey = UserManager::generateActivationKey($model->password);
                        $model->save(true, ['password']);
                        Yii::app()->user->setFlash('profileMessage', "New password is saved.");
                        $controller->redirect(array("user/profile"));
                        return;
                    }
                    else
                    {
                        $form->addError('oldPassword','Old password is incorrect.');
                    }
                }
            }
            $controller->render('changePassword',array('form'=>$form));
        }
    }
} 