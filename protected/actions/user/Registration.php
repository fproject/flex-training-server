<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 * User Account Registration Action
 *
 * @property UserController $controller
 *
 */
class Registration extends CAction
{
    public function run()
    {
        /** @var UserController $controller */
        $controller = $this->getController();

        $model = new UserRegistrationForm;
        $profile=new UserProfile;
        $profile->regMode = true;

        // ajax validator
        if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
        {
            echo CActiveForm::validate(array($model,$profile));
            Yii::app()->end();
        }

        if (Yii::app()->user->id)
        {
            $controller->redirect(Yii::app()->userManager->profileUrl);
        }
        else
        {
            if(isset($_POST['UserRegistrationForm']))
            {
                $model->attributes=$_POST['UserRegistrationForm'];
                $profile->attributes=((isset($_POST['UserProfile'])?$_POST['UserProfile']:array()));
                $srcPassword = $model->password;

                if($model->validate()&&$profile->validate())
                {
                    $model->activateKey=UserManager::generateActivationKey($model->password);

                    $model->isSuperuser=0;
                    $model->status=(Yii::app()->userManager->activateAfterRegister? User::STATUS_ACTIVATED : User::STATUS_INACTIVATED);

                    $transaction = Yii::app()->db->beginTransaction();

                    if ($model->save(false))
                    {
                        $profile->userId=$model->id;
                        $profile->save();

                        $success = true;
                        if (Yii::app()->userManager->sendActivationMail)
                        {
                            $activationUrl = $controller->createAbsoluteUrl('/user/activation',array("activateKey" => $model->activateKey, "email" => $model->email));
                            $success = EmailHelper::sendMail($model->email,"You registered from ".Yii::app()->name,"Please activate you account go to ".$activationUrl);
                        }

                        if($success)
                        {
                            $transaction->commit();

                            if ((Yii::app()->userManager->allowInactivatedLogin||
                                    (Yii::app()->userManager->activateAfterRegister&&Yii::app()->userManager->sendActivationMail==false))
                                &&Yii::app()->userManager->autoLogin)
                            {
                                $identity=new UserIdentity($model->userName,$srcPassword);
                                $identity->authenticate();
                                Yii::app()->user->login($identity,0);
                                $controller->redirect(Yii::app()->userManager->profileUrl);
                            }
                            else
                            {
                                if (!Yii::app()->userManager->activateAfterRegister&&!Yii::app()->userManager->sendActivationMail)
                                {
                                    Yii::app()->user->setFlash('registration',"Thank you for your registration. Contact Admin to activate your account.");
                                }
                                elseif(Yii::app()->userManager->activateAfterRegister&&Yii::app()->userManager->sendActivationMail==false)
                                {
                                    Yii::app()->user->setFlash('registration',"Thank you for your registration. Please ".CHtml::link('Login',Yii::app()->userManager->loginUrl).".");
                                }
                                elseif(Yii::app()->userManager->allowInactivatedLogin)
                                {
                                    Yii::app()->user->setFlash('registration',"Thank you for your registration. Please check your email or login.");
                                }
                                else
                                {
                                    Yii::app()->user->setFlash('registration',"Thank you for your registration. Please check your email.");
                                }
                                $controller->refresh();
                            }
                        }
                        else
                        {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('registration',"An error occurred while completing your registration. Please try again later.");
                        }

                    }
                    else
                        $transaction->rollback();

                } else $profile->validate();
            }
            $controller->render('/user/registration',array('model'=>$model,'profile'=>$profile));
        }
    }
} 