<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 * User Account Recovery Action
 *
 * @property UserController $controller
 *
 */
class Recovery extends CAction
{
    public function run()
    {
        /** @var UserController $controller */
        $controller = $this->getController();

        if (Yii::app()->user->id)
        {
            $controller->redirect(Yii::app()->params['loginSuccessUrl']);
        }
        else
        {
            $email = ((isset($_GET['email']))?$_GET['email']:'');
            $activateKey = ((isset($_GET['activateKey']))?$_GET['activateKey']:null);
            if ($email&&$activateKey)
            {
                /** @var User $find */
                $find = User::model()->unsafe()->findByAttributes(array('email'=>$email));
                if(isset($find) && UserManager::validateActivationKey($activateKey,$find->activateKey))
                {
                    $changePasswordForm = new UserChangePasswordForm();
                    if(isset($_POST['UserChangePasswordForm']))
                    {
                        $changePasswordForm->attributes=$_POST['UserChangePasswordForm'];
                        if($changePasswordForm->validate())
                        {
                            $find->password = $changePasswordForm->password;
                            $find->activateKey=UserManager::generateActivationKey($changePasswordForm->password);
                            if ($find->status==User::STATUS_INACTIVATED)
                            {
                                $find->status = User::STATUS_ACTIVATED;
                            }

                            //Call validation to encrypt password
                            $find->validate(['activateKey','password']);

                            $find->update(['status','activateKey','password']);
                            Yii::app()->user->setFlash('recoveryMessage',"Your password is successfully updated. Click ".CHtml::link("here",["site/login"]).' to login.');
                            $controller->redirect(Yii::app()->userManager->recoveryUrl);
                        }
                    }
                    $controller->render('resetPassword',array('form'=>$changePasswordForm));
                }
                else
                {
                    Yii::app()->user->setFlash('recoveryMessage',"Incorrect recovery link.");
                    $controller->redirect(Yii::app()->userManager->recoveryUrl);
                }
            }
            else
            {
                $recoveryForm = new UserRecoveryForm;
                if(isset($_POST['UserRecoveryForm']))
                {
                    $recoveryForm->attributes=$_POST['UserRecoveryForm'];
                    if($recoveryForm->validate())
                    {
                        /** @var User $user */
                        $user = User::model()->unsafe()->findbyPk($recoveryForm->userId);

                        $activation_url = 'http://' . $_SERVER['HTTP_HOST'].
                            $controller->createUrl(implode(Yii::app()->userManager->recoveryUrl),array("activateKey"=>$user->activateKey, "email"=>$user->email));

                        $subject = "You have requested a password recovery for the site ".Yii::app()->name;
                        $message = "You have requested a password recovery for the site ".Yii::app()->name.". To receive a new password, go to ".$activation_url.".";

                        if(EmailHelper::sendMail($user->email,$subject,$message))
                        {
                            Yii::app()->user->setFlash('recoveryMessage',"Please check your email. An instructions was sent to your email address.");
                            $controller->refresh();
                        }
                        else
                        {
                            Yii::app()->user->setFlash('registration',"An error occurred while making your password change request. Please try again later.");
                        }
                    }
                }
                $controller->render('recovery',array('model'=>$recoveryForm));
            }
        }
    }
} 