<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 *
 * User Profile Action
 *
 * @property UserController $controller
 *
 */
class Profile extends CAction
{
    public function run()
    {
        /** @var UserController $controller */
        $controller=$this->controller;
        $model = $controller->loadModel();
        $controller->render('profile',array(
            'model'=>$model,
            'profile'=>$model->userProfile,
        ));
    }
} 