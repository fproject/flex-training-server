<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 *
 * This Action class is used to open main Flex application
 *
 * @property MainAppController $controller
 *
 */
class Open extends CAction
{
    public function run()
    {
        /** @var AppContextData $appContext  */
        $appContext = Yii::app()->user->getAppContext();
        if(isset($appContext))
            $projectId = $appContext->workingProjectId;
        else
            $projectId = null;
        $this->controller->render('open',
            array('projectId'=>$projectId,
                'loginUserId'=>Yii::app()->user->id,
                'userToken'=>Yii::app()->user->token));
    }
} 