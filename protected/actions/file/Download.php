<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 *
 * This Action class is used to download files
 *
 * @property FileController $controller
 *
 */
class Download extends CAction
{
    public function run()
    {
        $fileId	= $_GET['fileId'];
        $filePath = FileHelper::getAttachmentBasePath().'/'.$fileId;
        if(file_exists($filePath))
        {
            Yii::app()->getRequest()->sendFile('PK', file_get_contents($filePath), 'multipart/form-data');
        }
        else
        {
            throw new Exception('Downloading file is not existed');
        }
    }
} 