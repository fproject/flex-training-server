<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 *
 * This Action class is used to get file from 'resources' folder by specifying file name
 *
 * @property FileController $controller
 *
 */
class FlexResource extends CAction
{
    public function run()
    {
        $fileName	= $_GET['fileName'];
        $filePath = FileHelper::getFlexResourceBasePath().'/'.$fileName;
        if(file_exists($filePath))
        {
            Yii::app()->getRequest()->sendFile('PK', file_get_contents($filePath), 'multipart/form-data');
        }
        else
        {
            throw new Exception('Flex resource does not exist');
        }
    }
} 