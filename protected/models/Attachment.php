<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/* ****************************************************************************
 *
 * This class is automatically generated and maintained by Gii.
 * Do not manually modify any line of the generated code.
 *
 * Your additional properties and methods must be placed at the bottom of
 * this class.
 *
 *****************************************************************************/
/**
 * This is the model class for table "f_attachment".
 *
 * The followings are the available columns in table 'f_attachment':
 * @property integer $id
 * @property string $name
 * @property integer $targetType
 * @property integer $targetId
 * @property string $fileType
 * @property string $fileId
 * @property integer $size
 * @property string $createTime
 * @property string $updateTime
 */
class Attachment extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'f_attachment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// @todo You should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('targetType, targetId, size', 'numerical', 'integerOnly'=>true),
			array('name, fileType, fileId', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('name, targetType, targetId, fileType, fileId, size, createTime, updateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// @todo You may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'targetType' => 'Target Type',
			'targetId' => 'Target',
			'fileType' => 'File Type',
			'fileId' => 'File',
			'size' => 'Size',
			'createTime' => 'Create Time',
			'updateTime' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name', $this->name,true);
		$criteria->compare('targetType', $this->targetType);
		$criteria->compare('targetId', $this->targetId);
		$criteria->compare('fileType', $this->fileType,true);
		$criteria->compare('fileId', $this->fileId,true);
		$criteria->compare('size', $this->size);
		$criteria->compare('createTime', $this->createTime,true);
		$criteria->compare('updateTime', $this->updateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Attachment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/* ************************************************************************
	 *
	 * ProjectKit.net implementation properties and methods are after this block
	 *
	 *********************************************************************** */


}