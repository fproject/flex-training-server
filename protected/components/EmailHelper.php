<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/**
 * The Email Helper class
 *
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
class EmailHelper {
    /**
     * Send mail method
     * @param string $email
     * @param string $subject
     * @param string $message
     * @return bool
     */
    public static function sendMail($email,$subject,$message)
    {
        $adminEmail = Yii::app()->params['adminEmail'];
        $headers = "MIME-Version: 1.0\r\nFrom: $adminEmail\r\nReply-To: $adminEmail\r\nContent-Type: text/html; charset=utf-8";
        $message = wordwrap($message, 70);
        $message = str_replace("\n.", "\n..", $message);

        $level = error_reporting(0); // Don't display errors
        $success = mail($email,'=?UTF-8?B?'.base64_encode($subject).'?=',$message,$headers);

        if($level != 0)
            error_reporting($level);
        return $success;
    }
} 