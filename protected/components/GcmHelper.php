<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/**
 * This is the helper class for sending notifications using GCM (Google Cloud Messaging).
 *
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
class GcmHelper
{
    /**
     * @var string $key
     */
    private $key = GcmHelper::DEFAULT_GCM_API_KEY;

    /**
     * @var string $url
     */
    private $url = 'https://android.googleapis.com/gcm/send';

    const DEFAULT_GCM_API_KEY = 'AIzaSyAueClACfjmvYkUEysbZtFE9EIWVYX5ckY';

	public function __construct()
	{
    
	}

    /**
     * Initialize the GCM information
     * @param string $key The Browser API key string for ProjectKit GCM account
     * The default value is API key of the 'projectkitnet' project created for account projectkitjsc@gmail.com
     * @return bool true
     */
    public function init($key=GcmHelper::DEFAULT_GCM_API_KEY)
    {
        $this->key = $key;
        return true;
    }

    /**
     * Send a GCM notification using curl
     * @param mixed $message The message data to send as the notification payload
     * @param array $registration_ids  An array of registration ids to send this notification to.
     * This ID is updated to 'f_gcm' table every time a user register his ProjectKit mobile
     * application to GCM by opening it for the first time.
     * @return mixed true on success or false on failure
     */
    public function push($message, $registration_ids)
    {
    	$fields = array(
						'registration_ids'  => $registration_ids,
						'data'              => $message,
						);
		
		$headers = array( 
							'Authorization: key=' . $this->key,
							'Content-Type: application/json'
						);
		
		// Open connection
		$ch = curl_init();
		
		// Set the url, number of POST vars, POST data

		curl_setopt( $ch, CURLOPT_URL, $this->url );
		
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			
		// Execute post
		$result = curl_exec($ch);
		
		// Close connection
		curl_close($ch);
		
		return $result;
    }
}