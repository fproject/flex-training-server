<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/**
 * This is the abstract base class for all form model classes of this application.
 * It is a customized model based on Yii CFormModel class.
 * All form model classes of this application should extend from this class.
 * @method array getMasterAttributeLabel(mixed $attribute, mixed $type) Get an item data from MasterValue text values,
 * make it available for label displaying in forms and views.
 *
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
abstract class FormModel extends CFormModel{
    /**
     * Override the behaviors() method of CModel class for master value handler
     * @return array
     */
    public function behaviors()
    {
        return array(
            'MasterListBehavior' => array(
                'class' => 'application.components.MasterListBehavior',
            ),
        );
    }
}