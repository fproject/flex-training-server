<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 *
 * The WebUser class is used for user authentication.
 * @property string $token
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
class XMLHelper {
    /**
     * Insert XML into a SimpleXMLElement
     *
     * @param SimpleXMLElement|DOMDocument $parent
     * @param string $xml
     * @param bool $before
     * @return bool XML string added
     */
    public static function simpleXmlInsert($parent, $xml, $before = false)
    {
        if($parent instanceof SimpleXMLElement)
        {
            // add the XML
            $node     = dom_import_simplexml($parent);
            $fragment = $node->ownerDocument->createDocumentFragment();
        }
        else
        {
            $node = $parent;
            $fragment = $parent->createDocumentFragment();
        }

        $fragment->appendXML($xml);

        if ($before) {
            return (bool)$node->parentNode->insertBefore($fragment, $node);
        }

        return (bool)$node->appendChild($fragment);
    }
} 