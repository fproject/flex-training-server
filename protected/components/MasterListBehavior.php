<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/**
 * This is the behavior class for all base classes (ActiveRecord, FormModel) of this application.
 * It is a customized behavior based on Yii CModelBehavior class.
 * All model base classes of this application should extend from this class.
 *
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
class MasterListBehavior extends CModelBehavior{
    private static $_masterListDataCache = array();

    /**
     * Get list data from MasterValue text values, make it available for
     * list controls in forms and views displaying (drop-down, options,...)
     *
     * In general, you should use controller's getMasterListData() method
     * rather than this method.
     * @param mixed $type The master type, e.g. 'IssueType', 'ProjectStatus',...
     * @return array The list data from MasterValue text values.
     */
    public function getMasterListData($type)
    {
        if (!isset(self::$_masterListDataCache[$type])) {
            $languageID = Yii::app()->language;
            $masterValues = MasterValue::model()->findAllByAttributes(
                array('typeId' => $type, 'languageId' => $languageID));
            self::$_masterListDataCache[$type] = CHtml::listData($masterValues, 'id', 'name');
        }

        return self::$_masterListDataCache[$type];
    }


    /**
     * Get an item data from MasterValue text values, make it available for
     * label displaying in forms and views.
     * @param mixed $attribute The attribute name of the model instance of which
     * the value is an ID of a master value record.
     * @param mixed $type The master type, e.g. 'IssueType', 'ProjectStatus',...
     * @return mixed The item data from MasterValue text values.
     */
    public function getMasterAttributeLabel($attribute, $type)
    {
        $masterValues = self::getMasterListData($type);
        return $masterValues[$this->getOwner()->attributes[$attribute]];
    }

    /**
     * Attach this behavior to an ActiveRecord instance
     * @param ActiveRecord $model
     */
    public static function attachTo($model)
    {
        if(is_null($model->asa("MasterListBehavior")))
        {
            $model->attachBehavior("MasterListBehavior",['class' => 'application.components.MasterListBehavior']);
        }
    }
}