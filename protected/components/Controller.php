<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/**
 * This is the abstract base class for all controller classes of this application.
 * It is a customized controller based on Yii CController class.
 * All controller classes of this application should extend from this class.
 *
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
abstract class Controller extends CController
{
    /* ************************************************************************
     *
     * Gii Generated properties and methods
     *
     *********************************************************************** */

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    const MODEL_ID_ISSUE = 0;

    /* ************************************************************************
     *
     * ProjectKit.net implementation properties and methods
     *
     *********************************************************************** */

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow everyone to use Captcha controller
                'actions' => array('captcha'),
                'users' => array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'controllers' => array('user'),
                'actions' => array('registration','activation','recovery'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to open main application
                'controllers' => array('mainApp'),
                'actions' => array('index','open'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to open main application
                'controllers' => array('file'),
                'actions' => array('download','flexResource'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'controllers' => array('xmlTest'),
                'actions' => array('index','create','update','delete'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'controllers' => array('jsonTest'),
                'actions' => array('index','create','update','delete'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'controllers' => array('wsTest'),
                'actions' => array('employee'),
                'users' => array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'controllers' => array('user', 'rights'),
                'actions' => array('index','view','create','update','admin','delete','install','authItem','profile','assignUser'),
                'users' => array('admin'),
            ),
            array('allow', // allow every user to perform update ther profile, password
                'controllers' => array('user'),
                'actions' => array('profile','update','changePassword'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'controllers' => array('mainApp', 'user','rights','file'),
                'users' => array('*'),
            ),
        );
    }
}