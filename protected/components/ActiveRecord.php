<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/**
 * This is the abstract base class for all AR model classes of this application.
 * It is a customized model based on Yii CActiveRecord class.
 * All model classes of this application should extend from this class.
 * @property string $createTime
 * @property integer $createUserId
 * @property string $updateTime
 * @property integer $updateUserId
 * @method array getMasterAttributeLabel(mixed $attribute, mixed $type) Get an item data from MasterValue text values,
 * make it available for label displaying in forms and views.
 *
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */

abstract class ActiveRecord extends CActiveRecord
{
    /**
     * Override the behaviors() method of CModel class for timestamp audition
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $timestampBehavior = [];

        if($this->hasAttribute('createTime'))
            $timestampBehavior['createAttribute'] = 'createTime';
        elseif($this->hasAttribute('createDate'))
            $timestampBehavior['createAttribute'] = 'createDate';
        else
            $timestampBehavior['createAttribute'] = null;

        if($this->hasAttribute('updateTime'))
            $timestampBehavior['updateAttribute'] = 'updateTime';
        elseif($this->hasAttribute('updateDate'))
            $timestampBehavior['updateAttribute'] = 'updateDate';
        else
            $timestampBehavior['updateAttribute'] = null;

        if(!empty($timestampBehavior) &&
            (isset($timestampBehavior['createAttribute']) || isset($timestampBehavior['updateAttribute'])))
        {
            $timestampBehavior['class'] = 'zii.behaviors.CTimestampBehavior';
            $behaviors = array_merge($behaviors,['CTimestampBehavior' => $timestampBehavior]);
        }

        return $behaviors;
    }

    /**
     * Prepares createUserId and updateUserId attributes before saving.
     */
    protected function beforeSave()
    {
        if ($this->hasAttribute('createUserId') && $this->isNewRecord)
        {
            if(null !== Yii::app()->user)
                $id = Yii::app()->user->id;
            if(isset($id))
                $this->createUserId = $id;
        }

        if($this->hasAttribute('updateUserId'))
        {
            if(!isset($id) && null !== Yii::app()->user)
                $id = Yii::app()->user->id;
            if(isset($id))
                $this->updateUserId = $id;
        }

        return parent::beforeSave();
    }

    protected function afterSave()
    {
        $timeAuditFields = ['createTime','updateTime','createDate','updateDate'];

        foreach($timeAuditFields as $field)
        {
            if ($this->hasAttribute($field))
            {
                //Sau khi save, tạm lấy thời gian của PHP server điền vào các trường auto-time.
                //Thực tế trong bản ghi CSDL các trường này đã được tự động điền theo giờ của DB server khi thực hiện lệnh save().
                //Ưu điểm là không phải truy vấn lại DB để lấy giá trị, nhược điểm là có sai số.
                $this->setAttribute($field, date(Yii::app()->params['dbDateTimeFormat'], time()));
            }
        }
        parent::afterSave();
    }
}