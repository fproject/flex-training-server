<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2014. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 * Define the constants use for comment target and attachment target
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
class TargetType {
    const PROJECT = 0;
    const TASK = 1;
    const ISSUE = 2;
    const RESOURCE = 3;
    const CALENDAR_ITEM = 4;
    const USER = 5;
    const POST = 6;
    const TASK_RESOURCE_ASSIGNMENT = 7;

    /**
     * Get a target type's name
     * @param $type the target type
     * @return string
     */
    public static function getTargetTypeName($type)
    {
        switch($type)
        {
            case PROJECT:
                return 'Project';
                break;
            case TASK:
                return 'Task';
                break;
            case ISSUE:
                return 'Issue';
                break;
            case RESOURCE:
                return 'Resource';
                break;
            case CALENDAR_ITEM:
                return 'CalendarItem';
                break;
            case USER:
                return 'User';
                break;
            case POST:
                return 'Post';
                break;
            default:
                return null;
                break;
        }
    }
} 