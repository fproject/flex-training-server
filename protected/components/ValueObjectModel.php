<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/**
 * The abstract base class for all AMF value-object model classes of this application.
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
abstract class ValueObjectModel
{
    private static $_staticInstances = array(); // class name => static instance
    /**
     * @var ActiveRecord[] the static AR model for this VO class
     */
    private static $_staticActiveRecords = array();
    /**
     * @var  bool $relationLoading indicate that all query without with() statement will use lazy loading
     */
    protected $relationLoading = false;

    /**
     * @var ActiveRecord the AR model for this VO instance
     */
    protected $_activeRecord;

    /**
     * Constructor.
     * @param ActiveRecord $activeRecord the activeRecord dedicated fo this VO
     * @param bool $relationLoading
     */
    public function __construct($activeRecord=null, $relationLoading=false)
    {
        $this->relationLoading = $relationLoading;
        if(!is_null($activeRecord))
        {
            $this->_activeRecord = $activeRecord;
            $this->populateFromAR($activeRecord, $relationLoading);
        }
    }

    /**
     * Returns the static AR model of the specified VO model class.
     * @param ActiveRecord $activeRecord the The AR model for this value-object class
     * @param string $className active record class name.
     * @return ValueObjectModel the static model class
     */
    public static function staticInstance($activeRecord = null, $className = __CLASS__)
    {
        if (isset(self::$_staticInstances[$className]))
            return self::$_staticInstances[$className];
        else {
            $model = self::$_staticInstances[$className] = new $className();
            self::$_staticActiveRecords[$className] = $activeRecord;
            return $model;
        }
    }

    /**
     * Find a single model object with the specified condition.
     * @param mixed $condition query condition or criteria.
     * If a string, it is treated as query condition (the WHERE clause);
     * If an array, it is treated as the initial values for constructing a {@link CDbCriteria} object;
     * Otherwise, it should be an instance of {@link CDbCriteria}.
     * @param array $params parameters to be bound to an SQL statement.
     * This is only used when the first parameter is a string (query condition).
     * In other cases, please use {@link CDbCriteria::params} to set parameters.
     * @return ValueObjectModel the model object found, converted to VO type. Null if no record is found.
     */
    public function find($condition = '', $params = array())
    {
        $class = get_class($this);
        /** @var $rec ActiveRecord */
        $rec = self::staticActiveRecord($class)->find($condition, $params);
        return $this->recordToModel($rec, $class, $this->relationLoading);
    }

    /**
     * @param string $className
     * @return ActiveRecord the AR model for this VO model
     */
    public static function staticActiveRecord($className = __CLASS__)
    {
        //Create static record instance as needed
        if (!isset(self::$_staticActiveRecords[$className]))
            call_user_func(array($className, 'staticInstance'));

        return self::$_staticActiveRecords[$className];
    }

    /**
     * Populate VO model properties from corresponding AR object.
     * @param ActiveRecord $ar the AR object
     * @param bool $relationLoading specify whether load relation field or not
     * @return void
     */
    protected function populateFromAR($ar, $relationLoading=true)
    {
        $this->relationLoading = false;
    }

    /**
     * Finds a single active record that has the specified attribute values.
     * See {@link find()} for detailed explanation about $condition and $params.
     * @param array $attributes list of attribute values (indexed by attribute names) that the active records should match.
     * An attribute value can be an array which will be used to generate an IN condition.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return ValueObjectModel the record found. Null if none is found.
     */
    public function findByAttributes($attributes,$condition='',$params=array())
    {
        $class = get_class($this);
        /** @var $record ActiveRecord */
        $record = self::staticActiveRecord($class)->findByAttributes($attributes, $condition, $params);
        return $this->recordToModel($record, $class, $this->relationLoading);
    }

    /**
     * Find all data model objects with the specified condition.
     * @param mixed $condition query condition or criteria.
     * If a string, it is treated as query condition (the WHERE clause);
     * If an array, it is treated as the initial values for constructing a {@link CDbCriteria} object;
     * Otherwise, it should be an instance of {@link CDbCriteria}.
     * @param array $params parameters to be bound to an SQL statement.
     * This is only used when the first parameter is a string (query condition).
     * In other cases, please use {@link CDbCriteria::params} to set parameters.
     * @return ValueObjectModel[] the search result models as value-objects.
     */
    public function findAll($condition = '', $params = array())
    {
        /** @var $recs ActiveRecord[] */
        $recs = $this->getActiveRecord()->findAll($condition, $params);
        return $this->recordsToModels($recs, $this->relationLoading);
    }

    /**
     * Create an instance of AR, populate with field values of this model
     * @param bool $useInstanceRecord if false, the static active record will be used.
     * Otherwise, the instance-dedicated active record will be used.
     * @return ActiveRecord
     */
    public function getActiveRecord($useInstanceRecord = false)
    {
        $class = get_class($this);
        if($useInstanceRecord)
        {
            if(is_null($this->_activeRecord))
            {
                $class = get_class(self::staticActiveRecord($class));
                $this->_activeRecord = new $class();
            }
            $this->populateToAR($this->_activeRecord);
            return $this->_activeRecord;
        } else {
            return self::staticActiveRecord($class);
        }
    }

    /**
     * Populate VO model properties to corresponding AR object.
     * @param ActiveRecord $ar the AR object
     */
    protected abstract function populateToAR($ar);

    /**
     * Convert an active record to a model
     * @param ActiveRecord $record
     * @param string $className the model class name
     * @param bool $relationLoading specify whether load relation field or not
     * @return ValueObjectModel the model that is constructed from active record
     */
    protected function recordToModel($record, $className = null, $relationLoading=true)
    {
        if(is_null($record))
            return null;
        if(is_null($className))
        {
            $className = get_class($this);
        }

        /** @var $model ValueObjectModel*/
        $model = new $className();
        $model->populateFromAR($record, $relationLoading);
        return $model;
    }

    /**
     * Convert an active array of records to an array of models
     * @param ActiveRecord[] $records
     * @param bool $relationLoading specify whether load relation field or not
     * @return ValueObjectModel[]
     */
    protected function recordsToModels($records, $relationLoading=true)
    {
        if(is_null($records))
            return null;
        $class = get_class($this);
        $models = array();
        foreach ($records as $rec) {
            $models[] = $this->recordToModel($rec, $class, $relationLoading);
        }
        return $models;
    }

    /**
     * Search and return model objects as value-objects.
     * The search condition is based on properties of current VO model.
     * @param array $attributes list of attribute values (indexed by attribute names)
     * that the active records should match.
     * An attribute value can be an array which will be used to generate an IN condition.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return ValueObjectModel[] the search result models as value-objects.
     * An empty array is returned if none is found.
     */
    public function findAllByAttributes($attributes, $condition = '', $params = array())
    {
        /** @var $records ActiveRecord[] */
        $records = $this->getActiveRecord()->findAllByAttributes($attributes, $condition, $params);
        return $this->recordsToModels($records, $this->relationLoading);
    }

    /**
     * Finds a data model object with the specified primary key.
     * See {@link find()} for detailed explanation about $condition and $params.
     * @param mixed $pk primary key value(s). Use array for multiple primary keys.
     * For composite key, each key value must be an array (column name=>column value).
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return ValueObjectModel the model object found. Null if none is found.
     */
    public function findByPk($pk, $condition = '', $params = array())
    {
        $class = get_class($this);
        /** @var $rec ActiveRecord */
        $rec = self::staticActiveRecord($class)->findByPk($pk, $condition, $params);
        return $this->recordToModel($rec, $class, $this->relationLoading);
    }

    /**
     * Finds all data models with the specified primary keys.
     * See {@link find()} for detailed explanation about $condition and $params.
     * @param mixed $pk primary key value(s). Use array for multiple primary keys.
     * For composite key, each key value must be an array (column name=>column value).
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return ValueObjectModel[] the search result models as value-objects. An empty array is returned if none is found.
     */
    public function findAllByPk($pk, $condition='', $params=array())
    {
        /** @var $records ActiveRecord[] */
        $records = $this->getActiveRecord()->findAllByPk($pk, $condition, $params);
        return $this->recordsToModels($records, $this->relationLoading);
    }

    /**
     * Finds the number of model objects satisfying the specified query condition.
     * See {@link find()} for detailed explanation about $condition and $params.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return string the number of model objects satisfying the specified query condition.
     * Note: type is string to keep max. precision.
     */
    public function count($condition='', $params=array())
    {
        return $this->getActiveRecord()->count($condition, $params);
    }

    /**
     * Finds the number of model objects that have the specified attribute values.
     * See {@link find()} for detailed explanation about $condition and $params.
     * @param array $attributes list of attribute values (indexed by attribute names) that the active records should match.
     * An attribute value can be an array which will be used to generate an IN condition.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return string the number of model objects satisfying the specified query condition.
     * Note: type is string to keep max. precision.
     * @since 1.1.4
     */
    public function countByAttributes($attributes, $condition='', $params=array())
    {
        return $this->getActiveRecord()->countByAttributes($attributes,$condition, $params);
    }

    /**
     * Checks whether there is model object satisfying the specified condition.
     * See {@link find()} for detailed explanation about $condition and $params.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return boolean whether there is row satisfying the specified condition.
     */
    public function exists($condition='', $params=array())
    {
        return $this->getActiveRecord()->exists($condition, $params);
    }

    /**
     * Specifies which related objects should be eagerly loaded.
     * This method takes variable number of parameters. Each parameter specifies
     * the name of a relation or child-relation. For example,
     * <pre>
     * // find all posts together with their author and comments
     * Post::staticInstance()->with('author','comments')->findAll();
     * // find all posts together with their author and the author's profile
     * Post::staticInstance()->with('author','author.profile')->findAll();
     * </pre>
     * The relations should be declared in {@link relations()}.
     *
     * By default, the options specified in {@link relations()} will be used
     * to do relational query. In order to customize the options on the fly,
     * we should pass an array parameter to the with() method. The array keys
     * are relation names, and the array values are the corresponding query options.
     * For example,
     * <pre>
     * Post::staticInstance()->with(array(
     *     'author'=>array('select'=>'id, name'),
     *     'comments'=>array('condition'=>'approved=1', 'order'=>'create_time'),
     * ))->findAll();
     * </pre>
     *
     * @return ValueObjectModel this VO itself.
     */
    public function with()
    {
        $this->relationLoading = true;
        $withParams = func_get_args();
        call_user_func(array($this->getActiveRecord(), 'with'), $withParams);
        return $this;
    }

    /**
     * Inserts a row into the table based on this active record attributes.
     * If the table's primary key is auto-incremental and is null before insertion,
     * it will be populated with the actual value after insertion.
     * Note, validation is not performed in this method. You may call {@link validate} to perform the validation.
     * After the record is inserted to DB successfully, its {@link isNewRecord} property will be set false,
     * and its {@link scenario} property will be set to be 'update'.
     * @param array $attributes list of attributes that need to be saved. Defaults to null,
     * meaning all attributes that are loaded from DB will be saved.
     * @return boolean whether the attributes are valid and the record is inserted successfully.
     * @throws CDbException if the row is not a new record
     */
    public function insert($attributes=null)
    {
        $b = $this->getActiveRecord(true)->insert($attributes);

        if($b)
            $this->afterSave();

        return $b;
    }

    /**
     * Insert a list of data to a table.
     * This method could be used to achieve better performance during insertion of the large
     * amount of data into the database table.
     * @param array $data list data to be inserted, each value should be an array in format (column name=>column value).
     * If a key is not a valid column name, the corresponding value will be ignored.
     * @return int number of rows inserted.
     */
    public function insertMultiple(array $data)
    {
        return DbHelper::insertMultiple($this->getActiveRecord()->tableName(), $data);
    }

    /**
     * Updates records with the specified primary key(s).
     * See {@link find()} for detailed explanation about $condition and $params.
     * Note, the attributes are not checked for safety and validation is NOT performed.
     * @param mixed $pk primary key value(s). Use array for multiple primary keys. For composite key,
     * each key value must be an array (column name=>column value).
     * @param array $attributes list of attributes (name=>$value) to be updated
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return integer the number of rows being updated
     */
    public function updateByPk($pk, $attributes, $condition='', $params=array())
    {
        return $this->getActiveRecord()->updateByPk($pk, $attributes, $condition, $params);
    }

    /**
     * Updates records with the specified condition.
     * See {@link find()} for detailed explanation about $condition and $params.
     * Note, the attributes are not checked for safety and no validation is done.
     * @param array $attributes list of attributes (name=>$value) to be updated
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return integer the number of rows being updated
     */
    public function updateAll($attributes, $condition='', $params=array())
    {
        return $this->getActiveRecord()->updateAll($attributes, $condition, $params);
    }

    /**
     * Updates the row represented by this model object.
     * All loaded attributes will be saved to the database.
     * Note, validation is not performed in this method. You may call {@link validate} to perform the validation.
     * @param array $attributes list of attributes that need to be saved. Defaults to null,
     * meaning all attributes that are loaded from DB will be saved.
     * @return boolean whether the update is successful
     * @throws CDbException if the record is new
     */
    public function update($attributes=null)
    {
        $ar = $this->getActiveRecord(true);
        $ar->setIsNewRecord(false);
        $b = $ar->update($attributes);

        if($b)
            $this->afterSave();

        return $b;
    }

    /**
     * Saves the current model.
     *
     * The model is inserted as a row into the database table if its primary key field
     * is null (usually the case when the record is created using the 'new'
     * operator). Otherwise, it will be used to update the corresponding row in the table
     * (usually the case if the record is obtained using one of those 'find' methods.)
     *
     * Validation will be performed before saving the record. If the validation fails,
     * the record will not be saved. You can call {@link getErrors()} to retrieve the
     * validation errors.
     *
     * If the record is saved via insertion, and if its primary key is auto-incremental,
     * the primary key will be populated with the automatically generated value.
     *
     * @param array $attributes list of attributes that need to be saved.
     * If this parameter is null, all attributes that are loaded from DB excepts for relations, will be saved.
     *
     * If value of first element of this array equals to '*', then all attributes that are not a relation will be saved,
     * and all attributes from the second element will be considered as relation names to be saved.
     *
     * If value of first element does not equal to '*', then all attributes will be saved regardless they are relation or not.
     *
     * @throws CDbException
     * @return boolean whether the saving succeeds
     */
    public function save($attributes=null)
    {
        $ar = $this->getActiveRecord(true);
        $this->removeTemporaryId($ar);
        $emptyPK = $this->getFirstValueNotSetPK($ar);
        $saveRelNames = [];
        $arRelations = $ar->relations();
        if(is_null($attributes) || $attributes[0] == '*')
        {
            $arAttributes = null;
            if(isset($attributes))
            {
                array_splice($attributes, 0, 1);
                foreach($attributes as $rel)
                {
                    if(array_key_exists($rel, $arRelations))
                    {
                        $saveRelNames[] = $rel;
                    }
                }
            }
        }
        else
        {
            $arAttributes = [];

            foreach($attributes as $attribute)
            {
                if(array_key_exists($attribute, $arRelations))
                {
                    $saveRelNames[] = $attribute;
                }
                else
                {
                    $arAttributes[] = $attribute;
                }
            }
        }

        if(isset($arAttributes) && count($arAttributes) == 0)
        {
            $b = true;
        }
        elseif(isset($emptyPK))
        {
            if($ar->insert($arAttributes))
            {
                $this->$emptyPK = $ar->$emptyPK;
                $b = true;
            } else
                $b = false;
        }
        else
        {
            $ar->setIsNewRecord(false);
            $b = $ar->update($arAttributes);
        }

        foreach($saveRelNames as $relName)
        {
            if(isset($emptyPK))
                $mode = self::SAVE_MODE_INSERT_ALL;
            else
                $mode = self::SAVE_MODE_AUTO;
            $this->saveRelation($relName, null, $mode);
        }

        if($b)
            $this->afterSave();

        return $b;
    }

    /**
     * @param ActiveRecord $ar
     */
    private function removeTemporaryId($ar)
    {
        $primaryKey = $ar->getTableSchema()->primaryKey;
        if(is_string($primaryKey))
        {
            $keyVal = $ar->getAttribute($primaryKey);
            if(!is_numeric($keyVal))
                $ar->setAttribute($primaryKey, null);
        }
    }

    /**
     * Return the first PK name that value of corresponding field in this VO is not set
     * @param ActiveRecord $ar
     * @return null|string
     */
    private function getFirstValueNotSetPK($ar)
    {
        $primaryKey = $ar->getTableSchema()->primaryKey;
        if(is_string($primaryKey))
        {
            if(!is_numeric($ar->getAttribute($primaryKey)))
                return $primaryKey;
        }
        elseif(is_array($primaryKey))
        {
            foreach($primaryKey as $pk)
            {
                if (!is_numeric($ar->getAttribute($pk)))
                {
                    return $pk;
                }
            }
        }

        return null;
    }

    /**
     * Return all PK names
     * @param ActiveRecord $ar
     * @return null|array
     */
    private function getAllPKNames($ar)
    {
        $primaryKey = $ar->getTableSchema()->primaryKey;
        if(is_string($primaryKey))
        {
            return [$primaryKey];
        }
        elseif(is_array($primaryKey))
        {
            return $primaryKey;
        }
        return null;
    }

    const SAVE_MODE_AUTO = 0;
    const SAVE_MODE_INSERT_ALL = 1;
    const SAVE_MODE_UPDATE_ALL = 2;

    /**
     * Save a list of data to a table, each row data may be inserted or updated depend on its existence.
     * This method could be used to achieve better performance during insertion/update of the large
     * amount of data to the database table.
     * @param ValueObjectModel[] $models list of models to be saved.
     * If a key is not a valid column name, the corresponding value will be ignored.
     * @param array $attributeNames name list of attributes that need to be update. Defaults to null,
     * meaning all fields of corresponding active record will be saved.
     * This parameter is ignored in the case of insertion
     * @param int $mode the save mode flag.
     * If this flag value is set to 0, any model that have a PK value is NULL will be inserted, otherwise it will be update.
     * If this flag value is set to 1, all models will be inserted regardless to PK values.
     * If this flag value is set to 2, all models will be updated regardless to PK values
     * @return array an array of two elements: the first is the last model ID (auto-incremental primary key)
     * inserted, the second is the number of rows inserted.
     * If there's no row inserted, the return value is null.
     */
    public function saveMultiple($models, $attributeNames=null, $mode=self::SAVE_MODE_AUTO)
    {
        if(is_null($models))
            return null;

        $ar = $this->getActiveRecord();
        $pkNames = $this->getAllPKNames($ar);
        $updateModels=[];
        $insertModels = [];
        if($mode==self::SAVE_MODE_INSERT_ALL)
        {
            $insertModels = $models;
        }
        elseif($mode==self::SAVE_MODE_UPDATE_ALL)
        {
            $updateModels = $models;
        }
        else
        {
            foreach ($models as $model)
            {
                if(property_exists($model,'_isInserting') && $model->_isInserting)
                {
                    $inserting = true;
                }
                else
                {
                    $inserting = false;
                    foreach($pkNames as $pkName)
                    {
                        if(property_exists($model,$pkName) && is_null($model->$pkName))
                        {
                            $inserting = true;
                            break;
                        }
                        elseif(property_exists($model,$pkName) && !is_numeric($model->$pkName))
                        {
                            $model->$pkName = null;
                            $inserting = true;
                            break;
                        }
                    }
                }

                if($inserting)
                    $insertModels[] = $model;
                else
                    $updateModels[] = $model;
            }
        }

        if(count($updateModels) > 0)
        {
            $data = $this->modelsToDataArray($ar, $updateModels, $attributeNames, $pkNames);
            return DbHelper::updateMultiple($ar->getTableSchema(), $data, $pkNames);
        }
        elseif(count($insertModels) > 0)
        {
            $data = $this->modelsToDataArray($ar, $insertModels);
            $count = DbHelper::insertMultiple($ar->getTableSchema(), $data);
            $lastPk = Yii::app()->db->commandBuilder->getLastInsertID($ar->getTableSchema());
            return array($lastPk, $count);
        } else
            return null;
    }

    /**
     * Convert an array of models to an array of data used for {@link saveMultiple()} method
     * @param ActiveRecord $record
     * @param ValueObjectModel[] $models
     * @param array $attributeNames name list of attributes that need to be saved. Defaults to null,
     * meaning all fields of corresponding active record will be saved.
     * @param mixed $pkNames Name or an array of names of primary key(s)
     * @return array
     */
    protected function modelsToDataArray($record, $models, $attributeNames = null, $pkNames = null)
    {
        $data = [];

        if(is_null($attributeNames))
        {
            $attributeNames = array_keys($record->attributes);
        }
        else
        {
            if(is_array($pkNames))
            {
                foreach($pkNames as $pk)
                {
                    if(!is_null($pk) && !StringHelper::inArray($pk, $attributeNames, false))
                    {
                        $attributeNames[] = $pk;
                    }
                }
            }
            else if(!is_null($pkNames) && !StringHelper::inArray($pkNames, $attributeNames, false))
            {
                $attributeNames[] = $pkNames;
            }
        }

        foreach ($models as $model)
        {
            $item = [];
            $model->populateToAR($record);
            foreach ($attributeNames as $attName)
            {
                if ($record->hasAttribute($attName))
                {
                    $columnName = $record->getTableSchema()->getColumn($attName)->name;
                    $item[$columnName] = $record->$attName;
                }
            }
            $data[] = $item;
        }
        return $data;
    }

    /**
     * Deletes the row corresponding to this active record.
     * @throws CDbException if the record is new
     * @return boolean whether the deletion is successful.
     */
    public function delete()
    {
        $ar = $this->getActiveRecord(true);
        $ar->setIsNewRecord(false);
        $ar->delete();
    }

    /**
     * Deletes rows with the specified primary key.
     * See {@link find()} for detailed explanation about $condition and $params.
     * @param mixed $pk primary key value(s). Use array for multiple primary keys. For composite key, each key value must be an array (column name=>column value).
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return integer the number of rows deleted
     */
    public function deleteByPk($pk,$condition='',$params=array())
    {
        $this->getActiveRecord()->deleteByPk($pk, $condition, $params);
    }

    /**
     * Deletes rows with the specified condition.
     * See {@link find()} for detailed explanation about $condition and $params.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return integer the number of rows deleted
     */
    public function deleteAll($condition='',$params=array())
    {
        $this->getActiveRecord()->deleteAll($condition, $params);
    }

    /**
     * Save a relation field of this VO
     * @param $relationName the name of relation
     * @param null $relationAttributeNames the attribute name of the relation class that will be saved
     * @param int $mode save mode.
     * @return array|bool|null
     */
    public function saveRelation($relationName,$relationAttributeNames=null, $mode=self::SAVE_MODE_AUTO)
    {
        $relation = $this->{$relationName};
        if(isset($relation))
        {
            $ar = $this->getActiveRecord(true);
            $primaryKey = $ar->getPrimaryKey();
            $activeRel = $ar->getActiveRelation($relationName);
            $b = !is_array($primaryKey) && isset($activeRel);

            /** @var ValueObjectModel $relationObject */
            if(is_array($relation))
            {
                if($b)
                {
                    /** @var ValueObjectModel $r */
                    foreach($relation as $r)
                    {
                        if(property_exists($r, $activeRel->foreignKey) && !is_numeric($r->{$activeRel->foreignKey}))
                            $r->{$activeRel->foreignKey} = $primaryKey;
                    }
                }

                $relationObject = $relation[0];
                return $relationObject->saveMultiple($relation, $relationAttributeNames, $mode);
            }
            else
            {
                $relationObject = $relation;
                if(property_exists($relationObject, $activeRel->foreignKey) && !is_numeric($relationObject->{$activeRel->foreignKey}))
                    $relationObject->{$activeRel->foreignKey} = $primaryKey;

                return $relationObject->save($relationAttributeNames);
            }
        }
        return null;
    }

    /**
     * This method is invoked after saving a record successfully.
     * The default implementation does nothing.
     * You may override this method to do postprocessing after record saving.
     */
    protected function afterSave()
    {
        //Abstract method, do nothing
    }
}