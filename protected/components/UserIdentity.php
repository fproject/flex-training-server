<?php
///////////////////////////////////////////////////////////////////////////////
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    const ERROR_USER_INACTIVATED=3;
    const ERROR_USER_BANNED=4;
    const ERROR_INVALID_TOKEN=5;

    private $_id;

    /**
     * Get the ID of logged in user.
     * @return int the ID of logged in user
     */
    public function getId()
    {
        return $this->_id;
    }

    /** @var string */
    public $token;

	/**
	 * Authenticates a user.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        if(isset($this->token))
        {
            return $this->authenticatePublicToken($this->name, $this->token);
        }

        /** @var User $user */
        $user=User::model()->unsafe()->findByNameOrEmail($this->username);
        if($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if(!$user->validatePassword($this->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else if($user->status==User::STATUS_INACTIVATED && Yii::app()->userManager->allowInactivatedLogin==false)
            $this->errorCode=self::ERROR_USER_INACTIVATED;
        else if($user->status==User::STATUS_BANNED)
            $this->errorCode=self::ERROR_USER_BANNED;
        else
        {
            $this->_id=$user->id;
            $this->username=$user->userName;

            //This attribute will be persisted throughout the session and can be stored in the Cookie
            //by setting CWebUser::allowAutoLogin to be true
            $user->saveAttributes(array(
                'lastLoginTime'=>date(Yii::app()->params['dbDateTimeFormat'], time()),
            ));

            UserManager::setUser($user);

            $this->setState('lastLoginTime', $user->lastLoginTime);

            $this->errorCode=self::ERROR_NONE;
        }

        if($this->errorCode==self::ERROR_NONE)
        {
            $this->createAndSaveTokenKeyToCache();
            return true;
        }
        else
            return false;
    }

    /**
     * @return string
     */
    private function createAndSaveTokenKeyToCache()
    {
        $expire = Yii::app()->user->tokenExpire;
        $tokenKey = uniqid(rand(), true);
        if(Yii::app()->cache->set($this->getTokenKeyCacheId($this->_id), $tokenKey, $expire))
        {
            $this->token = $this->createPublicToken($tokenKey);
        }
    }

    /**
     * @param string $userId
     * @return string
     */
    private function getTokenKeyCacheId($userId)
    {
        return "loginUserTokenKey_".$userId;
    }

    private static $IV = '1324567809123456';

    /**
     * @param string $tokenKey
     * @return string
     */
    private function createPublicToken($tokenKey)
    {
        if(is_null($tokenKey))
            return null;

        $tokenObj = new stdClass();
        $tokenObj->key = $tokenKey;
        $tokenObj->userId = $this->id;

        $method = Yii::app()->params['encryptionMethod'];
        $password = Yii::app()->params['encryptionPassword'];

        return openssl_encrypt(json_encode($tokenObj), $method, $password, false, self::$IV);
    }

    /**
     * Authenticate a public token string
     * @param mixed $userId
     * @param string $token
     * @return bool
     */
    private function authenticatePublicToken($userId, $token)
    {
        $this->errorCode = self::ERROR_INVALID_TOKEN;
        if(!is_null($token) && !is_null($userId))
        {
            $method = Yii::app()->params['encryptionMethod'];
            $password = Yii::app()->params['encryptionPassword'];
            $jsonStr = openssl_decrypt($token, $method, $password, false, self::$IV);
            $jsonObj = json_decode($jsonStr);
            if(!is_null($jsonObj) && !is_null($jsonObj->userId) && $jsonObj->userId == $userId)
            {
                $cacheId = $this->getTokenKeyCacheId($userId);
                $tokenKey = Yii::app()->cache->get($cacheId);
                if($tokenKey != false)
                {
                    if($tokenKey == $jsonObj->key)
                    {
                        //Refresh cache's expire time of this token
                        $expire = Yii::app()->user->tokenExpire;
                        if(Yii::app()->cache->set($cacheId, $tokenKey, $expire))
                            $this->errorCode = self::ERROR_NONE;
                    }
                }
            }
        }

        return $this->errorCode == self::ERROR_NONE;
    }
}