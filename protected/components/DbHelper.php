<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/**
 * The Database Helper class
 *
 * @author Bui Sy Nguyen <nguyenbs@gmail.com>
 */
class DbHelper {

    /**
     * Inserts a row into a table based on attributes.
     * @param string $table the table to insert
     * @param array $attributes list of attributes that need to be saved.
     * @return boolean whether the attributes are valid and the record is inserted successfully.
     */
    public static function insert($table, $attributes)
    {
        return 0 < Yii::app()->db->createCommand()->insert($table,$attributes);
    }

    /**
     * Insert a list of data to a table.
     * This method could be used to achieve better performance during insertion of the large
     * amount of data into the database table.
     * @param string $table the table schema ({@link CDbTableSchema}) or the table name (string).
     * @param array $data list data to be inserted, each value should be an array in format (column name=>column value).
     * If a key is not a valid column name, the corresponding value will be ignored.
     * @return int number of rows inserted.
     */
    public static function insertMultiple($table, $data)
    {
        Yii::trace('insertMultiple()','application.DbHelper');
        $command = Yii::app()->db->commandBuilder->createMultipleInsertCommand($table, $data);
        return $command->execute();
    }

    /**
     * Update a list of data to a table.
     * This method could be used to achieve better performance during updating of the large
     * amount of data to the database table.
     * @param mixed $table the table schema ({@link CDbTableSchema}) or the table name (string).
     * @param array $data list data to be inserted, each value should be an array in format (column name=>column value).
     * If a key is not a valid column name, the corresponding value will be ignored.
     * @param mixed $pkNames Name or an array of names of primary key(s)
     * @return int number of rows updated.
     */
    public static function updateMultiple($table, $data, $pkNames)
    {
        Yii::trace('updateMultiple()','application.DbHelper');
        $command = self::createMultipleUpdateCommand($table, $data, $pkNames);
        return $command->execute();
    }

    /**
     * Creates a multiple INSERT command with ON DUPLICATE KEY UPDATE statement.
     * This method compose the SQL expression via given part templates, providing ability to adjust
     * command for different SQL syntax.
     * @param mixed $table the table schema ({@link CDbTableSchema}) or the table name (string).
     * @param array $data list data to be saved, each value should be an array in format (column name=>column value).
     * If a key is not a valid column name, the corresponding value will be ignored.
     * @param mixed $pkNames Name or an array of names of primary key(s)
     * @param array $templates templates for the SQL parts.
     * @throws CDbException
     * @return CDbCommand multiple insert command
     */
    private static function createMultipleUpdateCommand($table, $data, $pkNames, array $templates=array())
    {
        $templates=array_merge(
            array(
                'rowUpdateStatement'=>'UPDATE {{tableName}} SET {{columnNameValuePairs}} WHERE {{rowUpdateCondition}}',
                'columnAssignValue'=>'{{column}}={{value}}',
                'columnValueGlue'=>', ',
                'rowUpdateConditionExpression'=>'{{pkName}}={{pkValue}}',
                'rowUpdateConditionJoin'=>' AND ',
                'rowUpdateStatementGlue'=>'; ',
            ),
            $templates
        );
        if(is_string($table) && ($table=Yii::app()->db->schema->getTable($tableName=$table))===null)
            throw new CDbException(Yii::t('yii','Table "{table}" does not exist.',
                array('{table}'=>$tableName)));
        $tableName=Yii::app()->db->commandBuilder->getDbConnection()->quoteTableName($table->name);
        $params=array();
        $quoteColumnNames=array();

        $columns=array();

        foreach($data as $rowData)
        {
            foreach($rowData as $columnName=>$columnValue)
            {
                if(!in_array($columnName,$columns,true))
                    if($table->getColumn($columnName)!==null)
                        $columns[]=$columnName;

            }
        }

        foreach($columns as $name)
            $quoteColumnNames[$name]=Yii::app()->db->commandBuilder->getDbConnection()->quoteColumnName($name);

        $rowUpdateStatements=array();
        $pkToColumnName=[];

        foreach($data as $rowKey=>$rowData)
        {
            $columnNameValuePairs=array();
            foreach($rowData as $columnName=>$columnValue)
            {
                if(is_array($pkNames))
                {
                    foreach($pkNames as $pk)
                    {
                        if (strcasecmp($columnName, $pk) == 0)
                        {
                            $params[':'.$columnName.'_'.$rowKey] = $columnValue;
                            $pkToColumnName[$pk]=$columnName;
                            continue;
                        }
                    }
                }
                else if (strcasecmp($columnName, $pkNames) == 0)
                {
                    $params[':'.$columnName.'_'.$rowKey] = $columnValue;
                    $pkToColumnName[$pkNames]=$columnName;
                    continue;
                }
                /** @var CDbColumnSchema $column */
                $column=$table->getColumn($columnName);
                $paramValuePlaceHolder=':'.$columnName.'_'.$rowKey;
                $params[$paramValuePlaceHolder]=$column->typecast($columnValue);

                $columnNameValuePairs[]=strtr($templates['columnAssignValue'],array(
                    '{{column}}'=>$quoteColumnNames[$columnName],
                    '{{value}}'=>$paramValuePlaceHolder,
                ));
            }

            //Skip all rows that don't have primary key value;
            if(is_array($pkNames))
            {
                $rowUpdateCondition = '';
                foreach($pkNames as $pk)
                {
                    if(!isset($pkToColumnName[$pk]))
                        continue;
                    if($rowUpdateCondition != '')
                        $rowUpdateCondition = $rowUpdateCondition.$templates['rowUpdateConditionJoin'];
                    $rowUpdateCondition = $rowUpdateCondition.strtr($templates['rowUpdateConditionExpression'], array(
                        '{{pkName}}'=>$pk,
                        '{{pkValue}}'=>':'.$pkToColumnName[$pk].'_'.$rowKey,
                    ));
                }
            }
            else
            {
                if(!isset($pkToColumnName[$pkNames]))
                    continue;
                $rowUpdateCondition = strtr($templates['rowUpdateConditionExpression'], array(
                    '{{pkName}}'=>$pkNames,
                    '{{pkValue}}'=>':'.$pkToColumnName[$pkNames].'_'.$rowKey,
                ));
            }

            $rowUpdateStatements[]=strtr($templates['rowUpdateStatement'],array(
                '{{tableName}}'=>$tableName,
                '{{columnNameValuePairs}}'=>implode($templates['columnValueGlue'],$columnNameValuePairs),
                '{{rowUpdateCondition}}'=>$rowUpdateCondition,
            ));
        }

        $sql=implode($templates['rowUpdateStatementGlue'], $rowUpdateStatements);

        //Must ensure Yii::app()->db->emulatePrepare is set to TRUE;
        $command=Yii::app()->db->commandBuilder->getDbConnection()->createCommand($sql);

        foreach($params as $name=>$value)
            $command->bindValue($name,$value);

        return $command;
    }
}