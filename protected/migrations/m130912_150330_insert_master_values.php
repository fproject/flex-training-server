<?php
Yii::import('application.models.*');
Yii::import('application.components.*');
include_once('TXDbMigration.php');
class m130912_150330_insert_master_values extends TXDbMigration
{
	public function up()
	{
        $this->executeFile('data/masterdata.sql');

        //f_user
        $this->insert('f_user',array('id'=>1,'userName'=>'admin','email'=>'training@f-project.net','displayName'=>'Administrator',
            'password'=>CPasswordHelper::hashPassword('admin'),'isSuperuser'=>1, 'status'=>1,'activateKey'=>UserManager::generateActivationKey('admin')));

        //f_user_profile
        $this->insert('f_user_profile', array("userId" => 1,"firstName" => "System","lastName" => "Administrator"));

        $this->insert('f_user_profile_field', array("id" => 1,"varName" => "firstName","title" => "First Name","fieldType" => "VARCHAR","fieldSize" => 255,
            "fieldSizeMin" => 3,"required" => 1,"errorMessage" => "Incorrect First Name (length between 3 and 50 characters).","position" => 1,"visible" => 1,
        ));

        $this->insert('f_user_profile_field', array("id" => 2,"varName" => "lastName","title" => "Last Name","fieldType" => "VARCHAR","fieldSize" => 255,
            "fieldSizeMin" => 3,"required" => 1,"errorMessage" => "Incorrect Last Name (length between 3 and 50 characters).","position" => 2,"visible" => 1,
        ));
	}

	public function down()
	{
        $this->truncateTable("f_master_value");
        $this->delete('f_user',array('id'=>1));
		return true;
	}
}