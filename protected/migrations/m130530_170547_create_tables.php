<?php

class m130530_170547_create_tables extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createMasterValueTable();
        $this->createUserTable();
        $this->createAppContextDataTable();
        $this->createUserProfileTable();
        $this->createUserProfileFieldTable();

        $this->createEmployeeTable();

        $this->createResourceTable();

        //Create foreign keys after creating all tables
        $this->createForeignKeys();

    }

    public function safeDown()
    {
        //Remove foreign keys
        $this->dropForeignKeys();

        //Truncate data
        $this->truncateTable('f_user');
        $this->truncateTable('f_master_value');
        $this->truncateTable('f_app_context_data');

        //Drop tables
        $this->dropTable('f_app_context_data');
        $this->dropTable('f_user');
        $this->dropTable('f_master_value');
    }

    //
    //      TABLES
    //

    private function createMasterValueTable()
    {
        $this->createTable('f_master_value', array(
            'id' => 'int(11) NOT NULL',
            'typeId' => 'string NOT NULL',
            'languageId' => 'string NOT NULL',
            'name' => 'string NOT NULL',
            'description' => 'text DEFAULT NULL',

            'createTime' => 'datetime DEFAULT NULL',
            'createUserId' => 'int(11) DEFAULT NULL',
            'updateTime' => 'datetime DEFAULT NULL',
            'updateUserId' => 'int(11) DEFAULT NULL',
        ), 'ENGINE=InnoDB');
    }

    private function createEmployeeTable()
    {
        $this->createTable('employee', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'age' => 'int(10) DEFAULT NULL',
            'gender' => 'tinyint NOT NULL DEFAULT 0',
        ), 'ENGINE=InnoDB');
    }

    private function createResourceTable()
    {
        $this->createTable('resource', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'note' => 'text DEFAULT NULL',
            //'lid' => 'int(11) DEFAULT NULL',
            //'outlineLevel' => 'int(11) DEFAULT NULL',
            'maxUnits' => 'decimal(14,4) DEFAULT NULL',
            'workCalendarId' => 'int(11) DEFAULT NULL',
            'materialLabel' => 'string DEFAULT NULL',
            'type' => 'tinyint DEFAULT 1',
            'group' => 'string DEFAULT NULL',
            'code' => 'string DEFAULT NULL',
            'standardRate' => 'decimal(14,4) DEFAULT NULL',
            'overtimeRate' => 'decimal(14,4) DEFAULT NULL',
            'costPerUse' => 'decimal(14,4) DEFAULT NULL',
            'userId' => 'int(11) DEFAULT NULL',//the user account associated with this resource in case of resource type is WORK
            'email' => 'string DEFAULT NULL',
            'createTime' => 'datetime DEFAULT NULL',
            'createUserId' => 'int(11) DEFAULT NULL',
            'updateTime' => 'datetime DEFAULT NULL',
            'updateUserId' => 'int(11) DEFAULT NULL',
            'xmlData' => 'mediumtext',
            'quantity' => 'decimal(14,4) DEFAULT NULL',
        ), 'ENGINE=InnoDB');
    }

    private function createUserTable()
    {
        //create the user table
        $this->createTable('f_user', array(
            'id' => 'pk',
            'userName' => 'string NOT NULL',
            'displayName' => 'string NOT NULL',
            'email' => 'string NOT NULL',
            'password' => 'string NOT NULL',
            'activateKey' => 'string DEFAULT NULL',
            'status' => 'tinyint NOT NULL DEFAULT 0',
            'isSuperuser' => 'boolean NOT NULL DEFAULT 0',
            'lastLoginTime' => 'datetime DEFAULT NULL',
            'createTime' => 'datetime DEFAULT NULL',
            'createUserId' => 'int(11) DEFAULT NULL',
            'updateTime' => 'datetime DEFAULT NULL',
            'updateUserId' => 'int(11) DEFAULT NULL',
        ), 'ENGINE=InnoDB');

        $this->createIndex('idx_user_username', 'f_user', 'userName', true);
        $this->createIndex('idx_user_email', 'f_user', 'email', true);
    }

    private function createUserProfileTable()
    {
        //create the user table
        $this->createTable('f_user_profile', array(
            'userId' => 'pk',
            'firstName' => 'string NOT NULL',
            'lastName' => 'string NOT NULL',
        ), 'ENGINE=InnoDB');
    }

    private function createUserProfileFieldTable()
    {
        //create the user table
        $this->createTable('f_user_profile_field', array(
            'id' => 'pk',
            'varName' => 'varchar(50) NOT NULL',
            'title' => 'varchar(255) NOT NULL',
            'fieldType' => 'varchar(50) NOT NULL',
            'fieldSize' => 'tinyint DEFAULT 0',
            'fieldSizeMin' => 'tinyint DEFAULT 0',
            'required' => 'boolean DEFAULT 0',
            'match' => 'varchar(255) DEFAULT NULL',
            'errorMessage' => 'varchar(255) DEFAULT NULL',
            "otherValidator" => "text",
            'range' => 'text DEFAULT NULL',
            'default' => 'varchar(255) DEFAULT NULL',
            'widget' => 'varchar(255) DEFAULT NULL',
            'widgetParams' => 'text DEFAULT NULL',
            'position' => 'tinyint DEFAULT 0',
            'visible' => 'boolean DEFAULT 0',
        ), 'ENGINE=InnoDB');
    }

    private function createAppContextDataTable()
    {
        $this->createTable('f_app_context_data', array(
            'id' => 'pk',
            'loginUserId' => 'int(11) DEFAULT NULL',
            //'workingProjectId' => 'int(11) DEFAULT NULL',
            //'locale' => 'string NOT NULL',
            'xmlData' => 'mediumtext',
        ), 'ENGINE=InnoDB');
    }

    //
    //      FOREIGN KEYS
    //

    private function createForeignKeys()
    {
        //foreign key relationships

        //the f_app_context_data.loginUserId is a reference to f_user.id
        $this->addForeignKey('fk_app_context_data_user', 'f_app_context_data',
            'loginUserId', 'f_user', 'id', 'CASCADE', 'CASCADE');

        //the f_user_profile.userId is a reference to f_user.id
        $this->addForeignKey("fk_user_profile_user", "f_user_profile", "userId",
            "f_user", "id", "CASCADE", "RESTRICT");

    }

    private function dropForeignKeys()
    {
        $this->dropForeignKey('fk_app_context_data_user', "f_app_context_data");

        $this->dropForeignKey("fk_user_profile_user", "f_user_profile");
    }
}