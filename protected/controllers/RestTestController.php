<?php
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net 2013. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
Yii::import('application.services.vo.*');
class RestTestController extends RestController
{
    public $modelClass = 'application.models.Employee';
}
