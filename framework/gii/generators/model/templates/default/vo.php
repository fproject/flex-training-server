<?php
/**
 * This is the template for generating the VO class of a specified table.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 */
?>
<?php echo "<?php\n"; ?>
///////////////////////////////////////////////////////////////////////////////
//
// Licensed Source Code - Property of ProjectKit.net
//
// © Copyright ProjectKit.net <?php echo date("Y"); ?>. All Rights Reserved.
//
///////////////////////////////////////////////////////////////////////////////
/* ****************************************************************************
 *
 * This class is automatically generated and maintained by Gii, be careful
 * when modifying it.
 *
 * Your additional properties and methods should be placed at the bottom of
 * this class.
 *
 *****************************************************************************/
/**
 * This is the ProjectKit.net VO class associated with the model "<?php echo $modelClass; ?>".
 */
class <?php echo 'F'.$modelClass; ?> extends ValueObjectModel
{
    /**
     * Map the ActionScript class '<?php echo $modelClass; ?>' to this VO class:
     */
    public $_explicitType = '<?php echo 'F'.$modelClass; ?>';

<?php foreach($voFields as $name=>$type): ?>
    /** @var <?php echo str_replace('{R}', '', $type).' $'.$name; ?> */
    public $<?php echo $name.";\n\n"; ?>
<?php endforeach; ?>
<?php if($isRelationTable) :?>
    /**
     * @var bool $_isInserting a boolean flag used for saveMultiple() method,
     * indicated that this model object need to be inserted instead of updated.
     */
    public $_isInserting;
<?php endif?>

    /**
     * Returns the static model of this VO class.
     * @param <?php echo $modelClass; ?> $activeRecord the The AR model for this VO class
     * @param string $className active record class name.
     * @return F<?php echo $modelClass; ?> the static model class
    */
    public static function staticInstance($activeRecord = null, $className=__CLASS__)
    {
        if (is_null($activeRecord))
            $activeRecord = <?php echo $modelClass; ?>::model();
        return parent::staticInstance($activeRecord, $className);
    }

    /**
     * Populate VO model properties from corresponding AR object.
     * @param <?php echo $modelClass; ?> $ar the AR object
     * @param bool $relationLoading specify whether load relation field or not
     * @return void
     */
    protected function populateFromAR($ar, $relationLoading=true)
    {
<?php foreach($voFields as $name=>$type): ?>
<?php if(substr($type,0, 3) != '{R}') :?>
        $this-><?php echo $name; ?> = <?php
        if(array_key_exists($name,$userDefinedTypeColumns))
        {
            echo 'is_null($ar->'.$name.') ? null : '.$userDefinedTypeColumns[$name]->fromAR.";\n";
        } elseif(strcmp($type,'bool') == 0)
        {
            echo '(bool)$ar->'.$name.";\n";
        } elseif(substr($type, -2) == '[]')
        {
            echo $type.'::staticInstance()->recordsToModels($ar->'.$name.");\n";
        } else
        {
            echo '$ar->'.$name.";\n";
        }?>
<?php endif?>
<?php endforeach; ?>

<?php if($this->hasRelation($voFields)) :?>
        if($relationLoading)
        {
<?php foreach($voFields as $name=>$type): ?>
<?php
            if(substr($type,0, 3) == '{R}')
            {
                if(substr($type, -2) == '[]')
                {
                    echo "\t\t\t".'if($ar->hasRelated(\''.$name.'\'))'."\n".
                        "\t\t\t\t".'$this->'.$name.' = '.substr(str_replace('{R}', '', $type), 0, -2).'::staticInstance()->recordsToModels($ar->'.$name.", true);\n".
                        "\t\t\telse\n".
                        "\t\t\t\t".'$this->'.$name." = null;\n";
                }
                else
                {
                    echo "\t\t\t".'if($ar->hasRelated(\''.$name.'\'))'."\n".
                        "\t\t\t\t".'$this->'.$name.' = '.str_replace('{R}', '', $type).'::staticInstance()->recordToModel($ar->'.$name.", null, true);\n".
                        "\t\t\telse\n".
                        "\t\t\t\t".'$this->'.$name." = null;\n";
                }
            }?><?php endforeach; ?>
        }
        parent::populateFromAR($ar, $relationLoading);
<?php endif?>
    }

    /**
     * Populate VO model properties from corresponding AR object.
     * @param <?php echo $modelClass; ?> $ar the AR object
     */
    protected function populateToAR($ar)
    {
<?php foreach($voFields as $name=>$type): ?>
<?php if(substr($type,0, 3) != '{R}') :?>
        <?php
        if(array_key_exists($name,$userDefinedTypeColumns) && property_exists($userDefinedTypeColumns[$name],'toAR'))
        {
            echo '$ar->'.$name.' = is_null($this->'.$name.') ? null : '.$userDefinedTypeColumns[$name]->toAR.";\n";
        }
        else
        {
            echo '$ar->'.$name.' = $this->'.$name.";\n";
        }
        ?>
<?php endif?>
<?php endforeach; ?>

<?php foreach($derivedExcludeFields as $relName=>$relField): ?>
        <?php echo '$ar->'.$relField.' = isset($this->'.$relName.') ? $this->'.$relName."->id : null;\n";?>
<?php endforeach; ?>
    }
<?php

    $autoFields = [];
    foreach($userDefinedTypeColumns as $field)
    {
        if(array_key_exists($field->name, $voFields) && $field->autoValue)
        {
            $autoFields[]= $field;
        }
    }

    if(!empty($autoFields))
    {
        echo "\n\tprotected function afterSave()\n";
        echo "\t{\n";
        foreach($autoFields as $field)
        {
            echo "\t\t\$this->".$field->name." = is_null(\$this->_activeRecord->".$field->name.") ? null : ".str_replace('$ar->','$this->_activeRecord->',$field->fromAR).";\n";
        }

        echo "\t}\n";
    }
?>

    /* ************************************************************************
     *
     * ProjectKit.net implementation properties and methods are after this block
     *
     *********************************************************************** */
<?php if(strcmp($voUserCode,'')):?>
    <?php echo "\r\n\t".$voUserCode; ?>
<?php endif?>

}